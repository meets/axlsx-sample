# railsのxlsx出力サンプル

## Gemfileへの追加
axlsxを使用する場合は以下のgemを追加してください。

    # Excelの出力
    gem 'axlsx'
    gem 'axlsx_rails'


## 公式ドキュメント
yard経由でドキュメントを参照してください。

    bundle exec yard server -g

バージョンに違いがある可能性がありますがrubydocに置いてあるものを参照しても良いでしょう。

* http://www.rubydoc.info/github/randym/axlsx/file/README.md


## 参考
* [axlsx (gem) で xlsx ファイルをダウンロード - Rails つまみぐい](http://rails.hatenadiary.jp/entry/2013/02/25/160344)
